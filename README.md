<img src="readme/img/thumbnail.png" align="center" title="Murrengan network"/>

[murrengan.ru](https://murrengan.ru/)

<a href="readme/en"><img src="readme/img/united_states_of_america_usa.png" height="25" width="30" title="English"></a>

### Этот репозиторий содержит код социальной сети Мурренган
Разработка ведется через [fork](https://git-scm.com/book/ru/v2/GitHub-Внесение-собственного-вклада-в-проекты) и пулл реквесты

### Текущие фичи

* Регистрация клиента. Подтверждение по почте и другие социальные сети
* Создание текстовых карточек
* Добавление и обрезка фото
* Древовидные комментарии
* Рейтинговая система лайк/дизлайк

### В разработке применяется:

* [Python 3.7](https://www.python.org/downloads/release/python-369/)
* [Node 12](https://nodejs.org/)
* [Django 3](https://www.djangoproject.com/) 
* [Vue](https://vuejs.org) 

### Установка
```bash
# Убедиться, что активировано виртуальное окружение с python 3.7

git clone https://gitlab.com/Murrengan/murr.git # копировать проект локально
pip install -r requirements.txt  # установка зависимостей python
python manage.py migrate # миграция (подготовка) базы данных
python manage.py prepare_stand # создать администратора и тестового муррена
npm i # установка зависимостей node

```

### Запуск
```bash
# первая консоль 
python manage.py runserver
# вторая консоль
npm run serve
```

### Docker
```bash
# windows - для entrypoint.sh поставить line separator LF вместо CRLF (можно сменить в pycharm)

chmod +x entrypoint.sh # cделать файл entrypoint.sh исполняемым
docker-compose -f docker-compose-prod.yml up --build
```

###### Для https получить origin_ca_rsa_root.pem и private_origin_ca_ecc_root.pem сертификаты у cloudflare.com и разместить их в ./nginx

# 🌟Поддержать проект🌟 
[donationalerts](http://bit.do/eWnnm)

# ❤

## Контакты

[Telegram](https://t.me/MurrenganChat)

[youtube](https://youtube.com/murrengan/)

[vk](https://vk.com/murrengan)

[murrengan](https://www.murrengan.ru/)
